// script.js

document.addEventListener('DOMContentLoaded', function() {
    // Get the new chat button element
    const newChatButton = document.getElementById('newChatButton');
  
    // Add a click event listener to the new chat button
    newChatButton.addEventListener('click', function() {
      // Perform actions when the new chat button is clicked
      openNewChat();
    });
  
    // Function to open a new chat (replace this with your actual implementation)
    function openNewChat() {
      // Add your code to open a new chat here
      alert('Opening a new chat!');
      // Replace the alert with your actual code to open a new chat
    }
  });

  
  // Inside your script tag or external JavaScript file

// Inside your script tag or external JavaScript file

// Function to handle the submit button click
// Inside your script tag or external JavaScript file

// Function to handle the submit button click
function handleSubmit() {
    // Get the entered text from the input box
    var enteredText = document.getElementById('chatInput').value;
  
    // Clear the input box after submission
    document.getElementById('chatInput').value = '';
  
    // Create a new chat container
    var chatContainer = document.createElement('div');
    chatContainer.classList.add('chat-container');
  
    // Add profile picture
    var profilePic = document.createElement('img');
    profilePic.src = 'https://media.licdn.com/dms/image/D4E03AQEd07Cv6Aue4w/profile-displayphoto-shrink_400_400/0/1696535733254?e=1706140800&v=beta&t=xjd0uvr3Uf-xOapdzLk27PkP_IaVOydNjswhY_5YWVk';
    profilePic.alt = 'Profile Picture';
    profilePic.classList.add('profile-pic');
    chatContainer.appendChild(profilePic);
  
    // Add name
    var name = document.createElement('span');
    name.classList.add('user-name');
    name.textContent = 'Niranjan';
    chatContainer.appendChild(name);
  
    // Add the entered text
    var chatText = document.createElement('p');
    chatText.classList.add('chat-text');
    chatText.textContent = enteredText;
    chatContainer.appendChild(chatText);
  
    // Display the chat container in the chat display
    document.getElementById('chatDisplay').appendChild(chatContainer);
  }
  
  // Attach the function to the submit button click event
  document.getElementById('submitButton').addEventListener('click', handleSubmit);
  
  